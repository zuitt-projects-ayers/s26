// Introduction to NodeJS

let http = require("http");
	// modules need to be imported or required.

console.log(http)
	// http module contains methods and other codes which allows us to create a server and let our client communicate thorugh HTTP.

// createServer() - It creates a server that handles requests and responses.
	// .createServer() - This method has an anonymous function that handles our client and our server response. The anonymouse function in the createServer method is able to receive two objects: first, req, or request, from the client; second, res, or response, is our server response.
	// Each reqest and response parameters are objects which contains the details of a request and response as well as the method to handle them.

	// res.writeHead() - A method of the response object.
	// This will allow us to add headers, which are additional information about our server's response. The first argument in wrtieHead is an HTTP status, which is used to tell the client about the status of the requests.

	// res.end() - is a method of a response object which ends the server's response and sends a message as a string.

	// .listen() - assigns a port to a server. There are several tasks and processes in our computer which are also designated into their specific ports.

	// http://localhost:4000
		// localhost - current machine
		// 4000 - port number assigned to where the process/server is listening/running from

	// (Only certain ports are available to use: 4000, 3000, 8080. Always be sure to close the server with CTRL - C in the terminal before closing the terminal.)

http.createServer((req, res) => {

	// localhost:4000/ - is called an endpoint
	if (req.url === "/"){
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end('Hi! Welcome to our Homepage!')

	// localhost:4000/login
	} else if (req.url === "/login"){
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end('Welcome to the login page!')
	} else {
		res.writeHead(404, {'Content-Type' : 'text/plain'})
		res.end('Resource cannot be found.')
	}
}).listen(4000);
console.log('Server is running on localhost: 4000');